# Php console script for image optimizatyon tinyPNG.com

Scan local files and optimize it with tinyPNG

### Instalation
* download repository **git clone https://armgono@bitbucket.org/voodookrd/tinypng-optimizer.git**
* run** composer install** 
* get api key form  [tinyPNG](https://tinypng.com/developers) 
* Set this key in config.php file
* Use convert.php for optimization
* profit

### Usage
Set api key in config.php file 

in console run 
```
#!bash

php convert.php --dir=/images/contain/dir/path
```