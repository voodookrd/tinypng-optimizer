<?php
namespace tpo;

class Optimizator {
	
	protected $files = array();

	private $api_key;
	private $dir;

	public function __construct($key, $dir = ''){
		$this->api_key = $key;
		$get = getopt('', array('dir:'));
		if(isset($get['dir']) && is_dir($get['dir'])){
			$this->dir = $get['dir'];
			$this->getImages($this->dir);
		}
	}
	public function getImages($dir){
		$dirDatas = scandir($dir);
		foreach($dirDatas as $filename){
			$file_path = $dir . '/' . $filename;
			if(is_file($file_path)){
				$mime = mime_content_type($file_path);
				if(($mime == 'image/jpeg') || ($mime == 'image/png')){
					$this->files[] = $file_path;
				}
			}
		}
	}

	public function convert(){
		try {
			\Tinify\setKey($this->api_key);
			\Tinify\validate();
		} catch(\Tinify\Exception $e) {
			echo "API key not valid";
		}
		echo "Start convertation\n";
		$total = count($this->files);
		$current = 0;
		foreach($this->files as $file){
			$filename = basename($file);
			$file_dir = dirname($file);
			$tpo_dir = $file_dir . '/optimized';
			if(!is_dir($tpo_dir)){
				mkdir($tpo_dir);
			}
			$new_file_name = $tpo_dir . '/' . $filename;

			try{
				$source = \Tinify\fromFile($file);
				$source->toFile($new_file_name);	
			}catch(\Tinify\AccountException $e){
				echo "The error message is: " + $e.getMessage();
			}catch(\Tinify\ClientException $e){
				echo "The error message is: " + $e.getMessage();
			}
			$current++;
			$precent = 100 * round($current / $total, 2);
			echo "Processed: {$current} of {$total} ({$precent}%) . File {$filename} optimized successfull\n";
		}
	}
}
