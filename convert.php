#!/usr/bin/env php
<?php
require_once("vendor/autoload.php");
require_once("config.php");
require_once("lib/tpo.class.php");
$tpo = new \tpo\Optimizator(TPO_API_KEY);
$tpo->convert();
